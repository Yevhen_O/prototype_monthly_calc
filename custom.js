// A $( document ).ready() block.
$( document ).ready(function() {

	calc_monthly_payment();

    $("#list_price, #installments").change(function() {
	   	calc_monthly_payment();
	});
});

function calc_monthly_payment(){
	list_price = $( "#list_price" ).val();
	installments = $( "#installments" ).val();
	if(list_price > 0){
		
			$("#graph_result").removeClass("empty");
			axis_x = installments/12*10;
			axis_x = axis_x+'%';
			if(installments != 0){
				axis_y = (list_price/installments * 90)/(list_price/12);
				axis_y = axis_y+'%';	
				monthly_payment = list_price/installments;
				if(monthly_payment > 1000){
					monthly_payment = addCommas(Math.round(monthly_payment/1000)*1000);
				} else {
					monthly_payment = monthly_payment.toFixed(1);
				}
			} else {
				dom_instalments = document.getElementById('installments');
				dom_instalments.value = 12;
				calc_monthly_payment();
			}			
			$("#axis").css("width", axis_x).css("height", axis_y);
			$("#legend").text('￥ '+monthly_payment).css("bottom", axis_y);

	} else {
		$("#graph_result").addClass("empty");
	}

}

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}